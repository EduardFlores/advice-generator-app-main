const button = document.getElementById('button');
const advice = document.getElementById('advici');
const adviceId = document.getElementById('advici-id');

document.addEventListener('DOMContentLoaded', () => {
  peticion();
});

button.addEventListener('click', () => {
  peticion();
});

const peticion = async () => {
  const url = 'https://api.adviceslip.com/advice';
  const response = await fetch(url);
  const data = await response.json();
  console.log(data);
  advice.textContent = data.slip.advice;
  adviceId.textContent = data.slip.id;
};
